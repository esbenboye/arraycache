<?php
class ArrayCache{
    public static $caches = [];
    public $name = "";
    public static function factory($name){
        
        if(!isset(self::$caches[$name])){
            self::$caches[$name] = [];
        }
        
        $cache = new self();
        $cache->name = $name;
        return $cache;
    }
    
    public function has($key){
        return isset(self::$caches[$this->name][$key]);
    }
    
    public function get($key){
        if(!$this->has($key)){
            return NULL;
        }else{
            return self::$caches[$this->name][$key];            
        }
    }
    
    public function getAll(){
        return self::$caches[$this->name];
    }
    
    public function set($key, $value){
        self::$caches[$this->name][$key] = $value;
    }
}